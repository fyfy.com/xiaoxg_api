<?php

namespace App\Task;

use App\Model\UsersModel;
use App\Utility\Pool\MysqlObject;
use App\Utility\Pool\MysqlPool;
use EasySwoole\Component\Pool\PoolManager;
use EasySwoole\EasySwoole\Config;
use EasySwoole\EasySwoole\Logger;
use EasySwoole\EasySwoole\Swoole\Task\AbstractAsyncTask;

class TaskTest extends AbstractAsyncTask
{
    protected function run($taskData,$taskId,$fromWorkerId,$flags = null){
        $model = new UsersModel();
        $res = $model->inc(1,'num');
        Logger::getInstance()->log($taskData,'task');
        return true;
    }


    function finish($result, $task_id){

        return true;
    }

}