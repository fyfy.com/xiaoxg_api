<?php

namespace App\Process;

use App\Model\UsersModel;
use App\Queue\Queue;
use App\Task\TaskTest;
use Carbon\Carbon;
use EasySwoole\EasySwoole\Logger;
use EasySwoole\Component\Process\AbstractProcess;
use EasySwoole\EasySwoole\Swoole\Task\TaskManager;

class MessagePush extends AbstractProcess
{
    private $isRun = false;
    public function run($arg){
        //定时500ms检测有没有任务，有的话就while死循环执行
        $this->addTick(500,function (){
            if(!$this->isRun){
                $this->isRun = true;
                for ($i = 1; $i <= 2; $i++) {
                    $queue = new Queue();
                    go(function () use ($queue) {
                        while (true) {
                            try {
                                $task = $queue->onQueue('message-push')->read();
                                if ($task) {
                                    $model = new UsersModel();
                                    $model->inc(1,'num');
                                    $model->update(['id'=>1,'login_at'=>Carbon::now()]);
                                } else {
                                    break;
                                }
                                unset($task);
                            } catch (\Throwable $throwable) {
                                break;
                            }
                        }
                        $this->isRun = false;
                    });
                    unset($queue);
                }
            }
        });
    }

    public function onShutDown()
    {
        // TODO: Implement onShutDown() method.
    }

    public function onReceive(string $str, ...$args)
    {
        // TODO: Implement onReceive() method.
    }
}
