<?php

namespace App\Utility;

/**
 * 生成文档类
 * Class ApiDoc
 * @package App\Utility
 */
class ApiDoc
{
    private $module;//模块名称
    private $path;//控制器路径
    private $namespace;//命名空间
    private $params = [];

    public function __construct($module)
    {
        $this->module = $module;
        $this->namespace = "\\App\\HttpController\\{$this->module}\\";
        $this->path = APP_PATH."HttpController/{$this->module}/";
    }

    public function parse($doc = '') {
        if($doc == ''){
            return $this->params;
        }
        if(preg_match('#^/\*\*(.*)\*/#s', $doc, $comment) === false){
            return $this->params;
        }
        $comment = trim($comment [1]);
        if(preg_match_all('#^\s*\*(.*)#m',$comment,$lines) === false){
            return $this->params;
        }
        $this->parseLines($lines[1]);
        return $this->params;
    }

    private function parseLines($lines) {
        foreach ( $lines as $line ) {
            $parsedLine = $this->parseLine($line);

            if ($parsedLine === false && ! isset ( $this->params ['description'] )) {
                if (isset ( $desc )) {
                    $this->params ['description'] = implode ( PHP_EOL, $desc );
                }
                $desc = array ();
            } elseif ($parsedLine !== false) {
                $desc [] = $parsedLine;
            }
        }
        $desc = implode ( ' ', $desc );
        if (! empty ( $desc ))
            $this->params ['long_description'] = $desc;
    }


    private function parseLine($line) {
        $line = trim($line);

        if (empty($line))
            return false; // Empty line

        if (strpos ( $line, '@' ) === 0) {
            if (strpos ( $line, ' ' ) > 0) {
                // Get the parameter name
                $param = substr ( $line, 1, strpos ( $line, ' ' ) - 1 );
                $value = substr ( $line, strlen ( $param ) + 2 ); // Get the value
            } else {
                $param = substr ( $line, 1 );
                $value = '';
            }
            if ($this->setParam ( $param, $value ))
                return false;
        }

        return $line;
    }
    private function setParam($param, $value) {

        if($param == 'param') {
            $arr = explode(' ',$value);
            $this->params [$param][] = $arr;
        }else if($param == 'return') {
            $arr = explode(' ',$value);
            $this->params [$param][] = $arr;
        }else if($param == 'request') {
            $this->params[$param] = 'https://baby.chumomei.com/'.$value;
        }else{
            $this->params[$param] = $value;
        }
        return true;
    }



    /**
     * 获得模块下的控制器列表
     * @return array
     */
    public function getControllerNameList()
    {
        $list = $this->getAllFiles($this->path);
        foreach($list as $key => $file){
            $list[$key] = basename($file, '.php');
            if( $list[$key] == '.' || $list[$key] == '..' || $list[$key] == $this->module ){
                unset($list[$key]);
            }
        }
        return array_values($list);
    }

    /**
     * 获取指定目录文件夹下所有的文件
     */
    private function getAllFiles($dir)
    {
        // 取出文件或者文件夹
        $list      = scandir( $dir );
        $files_arr = [];
        foreach($list as $key => $file){
            $location_dir = $dir.$file;
            $files_arr[]  = $location_dir;
            // 判断是否是文件夹 是就调用自身函数再去进行处理
            if( is_dir($location_dir.'/') && '.' != $file && '..' != $file ){
                $files_arr = array_merge_recursive($files_arr, $this->getAllFiles($location_dir.'/') );
            }
        }
        return $files_arr;
    }


    public function getControllerName($name){
        $class = new \ReflectionClass($this->namespace.$name); //建立Person这个类的反射类
//        $methods = $class->getMethods();//获取所有方法
//        $reflectionMethod = $class->getMethod('index');
        $doc = $class->getDocComment();
        return $this->parse($doc);
    }


}
