<?php
/**
 * Created by PhpStorm.
 * User: Da Xiong
 * Date: 2019/1/29
 * Time: 14:24
 */

namespace App\Model;


class RecordsModel extends BaseModel
{
    protected $table = 'records';

    protected $fillable = [
        'id','title','template_id','number','total','created_at','updated_at'
    ];
}
