<?php
/**
 * Created by PhpStorm.
 * User: Da Xiong
 * Date: 2019/1/28
 * Time: 15:12
 */

namespace App\Model;


class CatesModel extends BaseModel
{
    protected $table = 'cates';

    protected $fillable = [
        'id','name','avatar'
    ];
}
