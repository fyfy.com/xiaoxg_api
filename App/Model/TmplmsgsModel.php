<?php
/**
 * Created by PhpStorm.
 * User: Da Xiong
 * Date: 2019/1/29
 * Time: 15:06
 */

namespace App\Model;


class TmplmsgsModel extends BaseModel
{
    protected $table = 'tmplmsgs';

    protected $fillable = [
        'id','title','template_id','page','keyword','emphasis'
    ];
    protected $casts = ['keyword'];
}
