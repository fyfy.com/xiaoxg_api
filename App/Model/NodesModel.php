<?php

namespace App\Model;

class NodesModel extends BaseModel{

    protected $table = 'nodes';

    protected $fillable = [
        'id','name','pid','pitch'
    ];

    protected $casts = ['pitch'];//字段转化



}
