<?php

namespace App\Model;

class AdminsModel extends BaseModel{

    protected $table = 'admins';

    const STATUS_1 = 1;
    const STATUS_2 = 2;
    const STATUS_TEXT = [
        self::STATUS_2 => '<span class="el-tag el-tag--danger">锁定</span>',
        self::STATUS_1 => '<span class="el-tag">正常</span>',
    ];

    protected $fillable = [
        'id','username','nickname','password','role_id','created_at','login_at','status'
    ];






}
