<?php

namespace App\Model;

class VideosModel extends BaseModel{

    protected $table = 'videos';

    protected $fillable = [
        'id','title','thumb','view','share','cate_id','banner'
        ,'userid','status','link','chanel_id','option'
        ,'rand_cate','created_at','updated_at'
    ];

    const STATUS_1 = 1;//正常
    const STATUS_2 = 2;//禁用
    const OPTION_1 = 1;//未推荐
    const OPTION_2 = 2;//推荐
    const BANNER_1 = 1;//轮播否
    const BANNER_2 = 2;//轮播是

    const STATUS_TEXT = [
        self::STATUS_1 => '<span class="el-tag">正常</span>',
        self::STATUS_2 => '<span class="el-tag el-tag--danger">锁定</span>',
    ];
    const OPTION_TEXT = [
        self::OPTION_1 => '<span class="el-tag">否</span>',
        self::OPTION_2 => '<span class="el-tag">是</span>',
    ];
    const BANNER_TEXT = [
        self::BANNER_1 => '<span class="el-tag">否</span>',
        self::BANNER_2 => '<span class="el-tag">是</span>',
    ];
}
