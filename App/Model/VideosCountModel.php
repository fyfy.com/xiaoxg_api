<?php
/**
 * Created by PhpStorm.
 * User: Da Xiong
 * Date: 2019/1/29
 * Time: 10:02
 */

namespace App\Model;


class VideosCountModel extends BaseModel
{
    protected $table = 'videos_count';

    protected $fillable = [
        'id','video_id','views','date','share_in','admin_id','created_at'
    ];

}
