<?php

namespace App\HttpController\Applet;

use App\HttpController\BaseController;
use App\Model\ConfigsModel;

class ConfigController extends BaseController {

    // 获取配置项
    public function index(){
        $request = $this->request();
        $name = trim($request->getRequestParam('name')) ?: '';
        if(!$name){
            return $this->writeJson(1,null,'参数错误');
        }
        $model = new ConfigsModel();
        $info = $model->where('name',$name)->first();
        return $this->writeJson(0,$info['value']);
    }
}
