<?php

namespace App\HttpController\Applet;

use App\HttpController\BaseController;
use App\Model\FormIdModel;
use App\Utility\MiniProgram;
use Carbon\Carbon;

class AuthController extends BaseController {

    public function index(){
        return $this->writeJson(0,'请不要偷看数据');
    }

    public function login(){
        $request = $this->request();
        $code = $request->getRequestParam('code');
        $mini = new MiniProgram();
        $session = $mini->session($code);
        return $this->writeJson(1,['user'=>$session]);
    }

    public function form(){
        $request = $this->request();
        $form_id = $request->getRequestParam('form_id') ?: '';
        $openid = $request->getRequestParam('openid') ?: '';
        if($form_id && ($form_id !='the formId is a mock one') && $openid){
            $form = [
                'formid'     => $form_id,
                'openid'     => $openid,
                'created_at' => Carbon::now(),
                'expires_at' => Carbon::now()->addHours(168)->timestamp,
            ];
            (new FormIdModel())->create($form);
            return $this->writeJson(0);
        }
        return $this->writeJson(1,'数据非法');
    }
}
