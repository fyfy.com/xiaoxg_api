<?php

namespace App\HttpController\Admin;

use App\HttpController\BaseController;
use App\Utility\ApiToken;
use EasySwoole\Http\Message\Status;

/**
 * 权限控制器
 * Class AuthController
 */
class AuthController extends BaseController
{
    protected $userid;

    public function onRequest(?string $action): ?bool
    {
        $header = $this->request()->getHeaders();
        if(!isset($header['authorization'])){
            $this->response()->withStatus(Status::CODE_UNAUTHORIZED);
            $this->response()->write('Sorry,Unauthenticated.');
            return false;
        }
        list ($bearer, $token) = explode(' ',$header['authorization'][0]);
        if(!$token){
            $this->response()->withStatus(Status::CODE_UNAUTHORIZED);
            $this->response()->write('Sorry,Unauthenticated.1');
            return false;
        }
        $auth = (new ApiToken())->sessionCheckToken($token);
        if($auth === false){
            // TODO: 这里仅仅是ajax的返回，针对正常的返回可能需要重新定义，待完成
            $this->response()->withStatus(Status::CODE_UNAUTHORIZED);
            $this->response()->write('Sorry,Unauthenticated.2');
            return false;
        }
        $this->userid = $auth;
        return parent::onRequest($action);
    }

    public function afterAction(?string $actionName): void
    {
        $this->userid = null;
        parent::afterAction($actionName);
    }

}
