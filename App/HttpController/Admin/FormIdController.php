<?php
/**
 * Created by PhpStorm.
 * User: Da Xiong
 * Date: 2019/1/29
 * Time: 14:13
 */

namespace App\HttpController\Admin;


use App\Model\FormIdModel;
use App\Model\RecordsModel;
use Carbon\Carbon;

class FormIdController extends AuthController
{

    /**
     * FormID统计
     */
    public function index(){
        $formModel = new FormIdModel();
        $total = $formModel->count(); // 统计表所有数据
        $usable = $formModel->where('expires_at' ,Carbon::now(),'>')->groupBy('openid')->count(); // 统计表可用数据
        $added = $formModel->whereDate('created_at',Carbon::now()->toDateString())->count(); // 统计表今日新增数据
        $list = [
            ['total' => $total, 'usable' => $usable, 'added' => $added],
        ];
        $recordsModel = new RecordsModel();
        $log = $recordsModel->orderBy('created_at','DESC')->take(10)->get();
        $lists['list'] = $list;
        $lists['log'] = $log;
        return $this->writeJson(0,$lists);
    }

}
