<?php

namespace App\HttpController\Admin;

use App\Model\RolesModel;
use Carbon\Carbon;
use EasySwoole\Validate\Validate;

class RoleController extends AuthController {

    /**
     * @title 角色列表
     * @description 获取角色列表
     * @method GET
     * @request manage/role
     * @param int page 页码 false 1
     * @param string kwd 角色名称 false ''
     * @return int id 编号
     * @return string name 角色名称
     * @return string note 角色描述
     * @return int status 角色状态
     * @return string status_text 角色状态注解
     * @return string created_at 添加时间
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\Option
     * @throws \EasySwoole\Mysqli\Exceptions\OrderByFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function index(){
        $request = $this->request();
        $page = $request->getRequestParam('page') ?: 1;
        $kwd = trim($request->getRequestParam('kwd')) ?: '';
        $model = new RolesModel();
        $list = $model->select('id,name,note,status,created_at')
            ->where(function ($query) use($kwd){
            if($kwd){
                $query->where('name','%'.$kwd.'%','like');
            }
        })->orderBy('created_at','ASC')
        ->paginate($page);
        $this->stateToText($list['data'],[
            'status' => $model::STATUS_TEXT,
        ]);
        return $this->writeJson(0,$list);
    }

    public function update(){
        $request = $this->request();
        $data = $request->getRequestParam();
        $model = new RolesModel();
        $validate = new Validate();
        $info = $model->where('id',$data['id'],'<>')->where('name',$data['name'])->first();
        $validate->addColumn('name')->required('请填写角色名称')->func(function () use($info){
            return empty($info);
        },'角色名称已存在');
        if($this->validate($validate)){
            if($data['id']){
                $model->update($data);
                return $this->writeJson(0,null,'编辑角色成功');
            }else{
                //新增
                $data['rules'] = [];
                $data['created_at'] = Carbon::now();
                $model->create($data);
                return $this->writeJson(0,null,'新增角色成功');
            }
        }else{
            return $this->writeJson(1, null, $validate->getError()->__toString());
        }
    }

    public function status(){
        $request = $this->request();
        $id = (int)$request->getRequestParam('id');
        $model = new RolesModel();
        $info = $model->find($id);
        if($info['status'] == $model::STATUS_1){
            $model->update(['id'=>$id,'status'=>$model::STATUS_2]);
        }else{
            $model->update(['id'=>$id,'status'=>$model::STATUS_1]);
        }
        return $this->writeJson(0);
    }

    public function destroy(){
        $request = $this->request();
        $id = $request->getRequestParam('id');
        $model = new RolesModel();
        if(is_array($id)){
            $model->whereIn('id',$id)->delete();
        }else{
            $model->destroy($id);
        }
        return $this->writeJson(0);
    }

    public function all(){
        $model = new RolesModel();
        $list = $model->where('status',$model::STATUS_1)->select('id,name')->get();
        return $this->writeJson(0,$list);
    }

    public function access(){

    }

    public function store(){

    }

}
