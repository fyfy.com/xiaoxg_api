<?php
/**
 * Created by PhpStorm.
 * User: Da Xiong
 * Date: 2019/1/29
 * Time: 11:00
 */

namespace App\HttpController\Admin;


use App\Model\AdminsModel;
use Carbon\Carbon;
use EasySwoole\Utility\Hash;

class ManagerController  extends AuthController
{
    /**
     * 获取管理员列表all
     * @return bool|void
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function index(){
        $adminModel = new AdminsModel();
        $lists = $adminModel->get();
        return $this->writeJson(0,$lists);
    }

    /**
     * 获取登录信息
     */
    public function info(){
        try{
            $adminInfo = new AdminsModel();
            $adminInfo->select(['id','username','nickname','role_id','login_at']);
            $adminInfo->where('id', $this->userid);
            $result = $adminInfo->first();
            return $this->writeJson(0,$result);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }

    /**
     * 获取管理员列表(id,nickname)
     * @return bool|void
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function list(){
        $adminModel = new AdminsModel();
        $lists = $adminModel->select('id,nickname')->get();
        return $this->writeJson(0,$lists);
    }

    /**
     * 新增 与 更新
     * @return bool
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function update(){
        $request = $this->request();
        $params = $request->getRequestParam();
        if(!isset($params['id']))$params['id'] = 0;
        if($this->userid !== 1 || $this->userid !== $params['id']){
            return $this->writeJson(1,null,'只能改自己的信息哦！');
        }
        $adminModel = new AdminsModel();
        if($params['id'] > 0){
            // 更新
            $find = $adminModel->find($params['id']);
            if(!$find){
                return $this->writeJson(1,null,'数据已经不存在了');
            }
            if(isset($params['password']) && $params['password'] !== ''){
                $params['password'] = Hash::makePasswordHash($params['password']);
            }else{
                unset($params['password']);
            }
            $isUpdate = $adminModel->update($params);
            if(!$isUpdate){
                return $this->writeJson(1,null,'更新可能失败了');
            }else{
                return $this->writeJson(0,null,'更新成功！');
            }
        }
        // 新增
        unset($params['id']);
        if(!isset($params['password']) || $params['password'] === ''){
            $params['password'] = Hash::makePasswordHash(123456);
        }
        $params['created_at'] = Carbon::now();
        $isSave = $adminModel->create($params);
        if(!$isSave){
            return $this->writeJson(1,null,'新增失败了--添加过程并不顺利！');
        }
        return $this->writeJson(0,null,'新增成功');
    }

    /**
     * 删除
     */
    public function destroy()
    {
        $request = $this->request();
        $id = (int)$request->getRequestParam('id')??0;
        if($id === 0){
            return $this->writeJson(1, null,'参数错误');
        }
        $videoModel = new AdminsModel();
        $isDelete = $videoModel->destroy($id);
        $one =  $videoModel->find($id);
        if(!$isDelete || $one){
            return $this->writeJson(1, null,'删除失败');
        }
        return $this->writeJson(0, null,'删除成功');
    }
}
