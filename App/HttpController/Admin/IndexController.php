<?php

namespace App\HttpController\Admin;

use App\Utility\Filesystem;

class IndexController extends AuthController {


    /**
     * @title 首页
     * @description 管理后台首页
     * @method GET
     * @request manage/main
     * @return string title 页面标题
     * @return html admin/main/index 模板地址
     */
    public function index(){
        $this->assign('title','首页');
        return $this->fetch('admin/main/index');
    }

    /**
     * 上传图片
     */
    public function upload(){
        try{
            $request = $this->request();
            $files = $request->getUploadedFile('pic');
            $return = (new Filesystem($files))->qiNiuAs();
            return $this->writeJson(0,$return);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }
}
