<?php
/**
 * Created by PhpStorm.
 * User: Da Xiong
 * Date: 2019/1/29
 * Time: 14:42
 */

namespace App\HttpController\Admin;


use App\Model\ConfigsModel;

class ConfigController extends AuthController
{

    /**
     * 获取msg(公众号url)配置信息
     */
    public function getMsg(){
        $configModel = new ConfigsModel();
        $list = $configModel->where('type','msg')->get();
        return $this->writeJson(0,$list);
    }

    /**
     * 更新配置项信息
     */
    public function update(){
        $request = $this->request();
        $params = $request->getRequestParam();
        $configModel = new ConfigsModel();
        if(isset($params['id']) && $params['id'] > 0){
            // 更新
            $find = $configModel->find($params['id']);
            if(!$find){
                return $this->writeJson(1,null,'error');
            }
            $isUpdate = $configModel->update($params);
            if($isUpdate){
                return $this->writeJson(0);
            }
            return $this->writeJson(1,null,'error');
        }else{
            // 新增
            if(!isset($params['name']) || $params['name'] == ''){
                return $this->writeJson(1,null,'name is null');
            }
            $isFind = $configModel->where('name',$params['name'])->first();
            if($isFind){
                return $this->writeJson(1,null,'NAME 已存在');
            }
            if(isset($params['id']))unset($params['id']);
            $result = $configModel->create($params);
            if($result){
                return $this->writeJson(0);
            }
        }
        return $this->writeJson(1,null,'error');
    }
}
