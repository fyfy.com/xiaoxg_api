<?php

namespace App\HttpController\Admin;

use App\Model\AdminsModel;
use Carbon\Carbon;
use EasySwoole\Utility\Hash;
use EasySwoole\Validate\Validate;

class OperateController extends AuthController {

    /**
     * @title 管理人员列表
     * @description 获取管理人员列表
     * @method GET
     * @request manage/operate
     * @param int page 页码 false 1
     * @param string kwd 用户名/用户昵称 false ''
     * @return int id 编号
     * @return string username 用户名
     * @return string nickname 用户昵称
     * @return int role_id 角色ID
     * @return int status 状态
     * @return string status_text 状态注解
     * @return string created_at 添加时间
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\Option
     * @throws \EasySwoole\Mysqli\Exceptions\OrderByFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function index(){
        $request= $this->request();
        $page   = $request->getRequestParam('page') ?: 1;
        $kwd    = $request->getRequestParam('kwd') ?: '';
        $status = $request->getRequestParam('status') ?: '';
        $model  = new AdminsModel();
        $list   = $model->select('admins.id,username,nickname,role_id,admins.status,admins.created_at,r.name')
            ->where(function ($query) use($kwd,$status){
                if($kwd){
                    $query->where('username','%'.$kwd.'%','like')
                        ->whereOr('nickname','%'.$kwd.'%','like');
                }
                if($status){
                    $query->where('admins.status',$status);
                }
            })->join('roles as r','r.id = admins.role_id')
            ->orderBy('admins.created_at')
            ->paginate($page);

        $this->stateToText($list['data'],[
            'status' => $model::STATUS_TEXT,
        ]);
        return $this->writeJson(0,$list);
    }

    public function update(){
        $request = $this->request();
        $data = $request->getRequestParam();
        $model = new AdminsModel();
        $validate = new Validate();
        $info = $model->where('id',$data['id'],'<>')->where('username',$data['username'])->first();
        $validate->addColumn('username')->required('请填写管理员账号')->func(function () use($info){
            return empty($info);
        },'管理员账号已存在');
        $validate->addColumn('password')->func(function () use($data){
            if($data['password'] && (strlen($data['password'])<6)){
                return false;
            }
            return true;
        },'密码长度不能小于6位数');
        $validate->addColumn('nickname')->required('请填写管理员昵称');
        $validate->addColumn('role_id')->required('请填写管理员角色');
        if($this->validate($validate)){
            if($data['id']){
                if($data['password']){
                    $data['password'] =  Hash::makePasswordHash($data['password']);
                }else{
                    unset($data['password']);
                }
                $model->update($data);
                return $this->writeJson(0,null,'编辑管理员信息成功');
            }else{
                //新增
                $data['status'] = $model::STATUS_1;
                $data['password'] = $data['password'] ?: Hash::makePasswordHash('123456');
                $data['created_at'] = Carbon::now();
                $model->create($data);
                return $this->writeJson(0,null,'新增管理员信息成功');
            }
        }else{
            return $this->writeJson(1, null, $validate->getError()->__toString());
        }
    }

    public function status(){
        $request = $this->request();
        $id = (int)$request->getRequestParam('id');
        $model = new AdminsModel();
        $info = $model->find($id);
        if($info['status'] == $model::STATUS_1){
            $model->update(['id'=>$id,'status'=>$model::STATUS_2]);
        }else{
            $model->update(['id'=>$id,'status'=>$model::STATUS_1]);
        }
        return $this->writeJson(0);
    }

    public function destroy(){
        $request = $this->request();
        $id = $request->getRequestParam('id');
        $model = new AdminsModel();
        if(is_array($id)){
            $model->whereIn('id',$id)->delete();
        }else{
            $model->destroy($id);
        }
        return $this->writeJson(0);
    }

}
