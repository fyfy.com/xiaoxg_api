<?php

namespace App\HttpController\Web;

use App\HttpController\BaseController;
use App\Model\FormIds;
use App\Queue\Queue;
use App\Task\TaskTest;
use EasySwoole\EasySwoole\Swoole\Task\TaskManager;

/**
 * @title 首页
 * @description 首页控制器
 * @method GET
 * @request go
 * @param int id 文章ID true 0
 * @param string code 验证码 false ''
 * @return int code 状态码（0成功1失败）
 * @return string msg 提示信息
 * @return int userid 用户ID
 * @example {title: "首页",description: "首页控制器",method: "GET",request: "https://baby.chumomei.com/go"}
 */
class IndexController extends BaseController {

    public function index(){
        return $this->writeJson(1,'这是测试go');
    }

    /**
     * 测试
     * @return bool
     */
    public function test(){
        return $this->writeJson(1,'这是测试go');
    }
}
