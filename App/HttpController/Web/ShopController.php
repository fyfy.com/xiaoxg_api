<?php

namespace App\HttpController\Web;

use App\HttpController\BaseController;
use App\Utility\ApiDoc;

class ShopController extends BaseController {


    public function index(){
        $apiDoc = new ApiDoc('Web');
        $list = $apiDoc->getControllerNameList();
        return $this->writeJson(1,$list);
    }
}
