<?php

namespace App\HttpController\Doc;

use App\HttpController\ViewController;
use App\Utility\ApiDoc;
use EasySwoole\EasySwoole\Config;

class IndexController extends ViewController {

    public function index() {
        //$url = Config::getInstance()->getConf("APP_URL");
        $url = Config::getInstance()->getConf("captcha");
        return $this->writeJson(200,$url);
        $mod = [
            ['title' => '管理后台','target' => $url.'doc/admin'],
            ['title' => '小程序','target' => $url.'doc/wechat']
        ];
        $this->assign('mod',$mod);
        $this->fetch('index');
    }

    public function admin(){
        $apiDoc = new ApiDoc('Web');
        $list = $apiDoc->getControllerName('IndexController');
        $this->assign('list',$list);
        $this->fetch('view');
    }

    public function wechat(){
        $apiDoc = new ApiDoc('Web');
        $list = $apiDoc->getControllerName('IndexController');
        $this->assign('list',$list);
        $this->fetch('view');
    }


}
