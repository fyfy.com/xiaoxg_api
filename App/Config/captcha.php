<?php

return [
    'charset'      => '123456789',              // 设置字符集合
    'length'       => 4,                        // 设置生成验证码位数
    'curve'        => false,                    // 开启混淆曲线
    'notice'       => true,                     // 开启噪点生成
    'font_color'   => '#E85850',                // 字体颜色
    'font_size'    => 26,                       // 字体大小
    'back_color'   => '#FFF0F5',                // 背景颜色
    'image_height' => 50,                       // 图片高度
    'image_weight' => 160,                      // 图片宽度
    'temp'         => EASYSWOOLE_ROOT . '/Temp',// 设置缓存目录
];
