<?php

use \App\HttpController\Router;

//管理后台
Router::group(['prefix'=>'manage','namespace'=>'Admin'],function (){
    //登录部分
    Router::post('login', 'LoginController/login');
    Router::get('logout', 'LoginController/logout');
    Router::get('info', 'ManagerController/info'); //获取登录信息
    Router::get('captcha', 'LoginController/captcha');
    //首页
    Router::get('', 'IndexController');
    //文件上传
    Router::post('upload', 'IndexController/upload');

    //管理人员
    Router::group(['prefix'=>'admin'],function (){
        Router::get('', 'ManagerController');
        Router::get('list', 'ManagerController/list');
        Router::post('update', 'ManagerController/update');
        Router::delete('destroy', 'ManagerController/destroy');
    });
//    Router::group(['prefix'=>'operate'],function (){
//        Router::get('', 'OperateController');
//        Router::post('update', 'OperateController/update');
//        Router::post('status', 'OperateController/status');
//        Router::post('destroy', 'OperateController/destroy');
//    });
    //角色
    Router::group(['prefix'=>'role'],function (){
        Router::get ('', 'RoleController');
        Router::post('update', 'RoleController/update');
        Router::post('destroy', 'RoleController/destroy');
        Router::get ('all', 'RoleController/all');
        Router::post('status', 'RoleController/status');
        Router::get ('access', 'RoleController/access');
        Router::post('store', 'RoleController/store');
    });
    //权限节点
    Router::group(['prefix'=>'node'],function (){
        Router::get('', 'NodeController');
        Router::post('update', 'NodeController/update');
        Router::post('destroy', 'NodeController/destroy');
    });

    //视频管理
    Router::group(['prefix' => 'video'], function(){
        Router::get ('','VideoController/index');//视频管理_列表
        Router::post ('update','VideoController/update');//视频管理_新增
        Router::delete('destroy','VideoController/destroy');//视频管理_删除
        Router::delete('destroy_all','VideoController/destroyAll');//视频管理_批量删除

        Router::get ('count','VideoController/count');//视频管理_统计
    });

    //分类管理
    Router::group(['prefix' => 'cate'], function(){
        Router::get('/','CategoryController/index');//分类管理_列表
        Router::post('update','CategoryController/update');//分类管理_更新
        Router::delete('destroy','CategoryController/destroy');//分类管理_删除
        Router::get('all','CategoryController/all');//分类管理_视频分类列表
    });

    //formID
    Router::group(['prefix' => 'formid'], function(){
        Router::get('/','FormIdController/index');//formid_列表
    });

    // 配置管理
    Router::group(['prefix' => 'config'], function(){
//        Router::get('/','ConfigController/index');//配置管理_列表——all
        Router::get('/msg','ConfigController/getMsg');//配置管理_公众号链接
        Router::post('update','ConfigController/update');//配置管理_更新
//        Router::delete('destroy','ConfigController/destroy');//分类管理_删除
    });

    // 模板消息 template
    Router::group(['prefix' => 'tmplmsg'], function(){
        Router::get('/','TemplateMsgController/index');//配置管理_列表——all
        Router::post('update','TemplateMsgController/update');//配置管理_更新
        Router::post('push','TemplateMsgController/push');//配置管理_更新
        Router::post('token','TemplateMsgController/token');//模板消息推送重置token
    });
});
