<?php

use \App\HttpController\Router;

Router::group(['prefix' => 'applet','namespace' => 'Applet'], function() {
    Router::get('', 'AuthController/index');
    Router::post('login', 'AuthController/login');//token
    Router::post('form','AuthController/form');//收集formid

    //视频列表
    Router::group(['prefix' => 'video'], function(){
        Router::get ('/',   'VideoController/index');//视频列表
        Router::get ('list','VideoController/randList');//视频详情列表
        Router::get ('info','VideoController/info');//视频详情
        Router::get ('banner','VideoController/getBanner');//视频轮播
    });
    //配置信息
    Router::get ('config','ConfigController/index');
});